import Ember from 'ember';

export default Ember.Controller.extend({
	action: {
		addUser: function() {
			var newUser = this.store.createRecord('user', {
				name: this.get('name'),
				surname: this.get('surname')
			});
			newUser.save();
			this.setProperties({
				name: '',
				surname: ''
			});
		}
	}
});
